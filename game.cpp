//
// Created by deca on 2/20/24.
//
#include "game.h"

Game::Game(const char* title, int width, int height) {
    if (initSDL() && createWindowAndRenderer(title, width, height)) {
        std::cout << "Game initialized successfully.\n";
        running = true;
    } else {
        std::cout << "Game initialization failed.\n";
        running = false;
    }
}

Game::~Game() {
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    TTF_Quit();
    SDL_Quit();
}

bool Game::initSDL() {
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cerr << "SDL could not initialize! SDL_Error: " << SDL_GetError() << '\n';
        return false;
    }
    if (TTF_Init() == -1) {
        std::cerr << "SDL_ttf could not initialize! TTF_Error: " << TTF_GetError() << '\n';
        return false;
    }
    return true;
}

bool Game::createWindowAndRenderer(const char* title, int width, int height) {
    window = SDL_CreateWindow(title, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_SHOWN);
    if (!window) {
        std::cerr << "Window could not be created! SDL_Error: " << SDL_GetError() << '\n';
        return false;
    }
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (!renderer) {
        std::cerr << "Renderer could not be created! SDL_Error: " << SDL_GetError() << '\n';
        return false;
    }
    return true;
}

void Game::handleInput() {
    SDL_Event event;
    while (SDL_PollEvent(&event) != 0) {
        if (event.type == SDL_QUIT) {
            running = false;
        } else if (event.type == SDL_KEYDOWN || event.type == SDL_KEYUP) {
            handleKeyEvent(event.key, event.type == SDL_KEYDOWN);
        }
    }
}

void Game::handleKeyEvent(SDL_KeyboardEvent& keyEvent, bool isKeyDown) {
    // Example: Quit game if Escape is pressed
    if (keyEvent.keysym.sym == SDLK_ESCAPE && isKeyDown) {
        running = false;
    }
    // Additional key event handling logic here
}

void Game::update(float deltaTime) {
    // Update game state and logic
}

void Game::render() {
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255); // Set the background color
    SDL_RenderClear(renderer); // Clear the screen

    // Render game objects here

    SDL_RenderPresent(renderer); // Update the screen
}

void Game::run() {
    const int FPS = 60;
    const int frameDelay = 1000 / FPS;
    Uint32 frameStart;
    int frameTime;

    while (running) {
        frameStart = SDL_GetTicks();

        handleInput();
        update(0); // Placeholder deltaTime. Compute actual deltaTime for game logic.
        render();

        frameTime = SDL_GetTicks() - frameStart;
        if (frameDelay > frameTime) {
            SDL_Delay(frameDelay - frameTime);
        }
    }
}