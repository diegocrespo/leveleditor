//
// Created by deca on 2/20/24.
//

#ifndef LEVELEDITOR_PLAYER_H
#define LEVELEDITOR_PLAYER_H

#include <SDL2/SDL.h>

struct Direction {
    bool up = false;
    bool down = false;
    bool left = false;
    bool right = false;
};

class Player {
public:

    Player(float initX, float initY, int initWidth, int initHeight, SDL_Color color, float speed, SDL_Renderer* renderer);
    ~Player();
    void draw();
    void update(float delta_time);
    Direction direction; // Member variable of type Direction
    void handleKeyEvent(SDL_Scancode scancode, bool is_key_down);
private:
    float x, y;
    int width, height;
    SDL_Color color;
    float speed;
    SDL_Renderer* renderer;

    //void handleKeyEvent(SDL_KeyboardEvent &event, bool is_key_down);

};

#endif //LEVELEDITOR_PLAYER_H
