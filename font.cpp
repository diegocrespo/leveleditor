//
// Created by deca on 2/20/24.
//
#include "font.h"
#include <iostream>

Font::Font(const char *font_path, const std::string &text, SDL_Color color, int size, SDL_Renderer *renderer) {
    font_renderer = renderer;
    font_color = color;
    font_text = text;

    font = TTF_OpenFont(font_path, size);
    if (!font) {
        std::cerr << "Failed to load font " << font_path << " with error: " << TTF_GetError() << std::endl;
        SDL_Quit();
        exit(1); // Or handle the error in a way that's appropriate for your application
    }

    font_surface = TTF_RenderText_Solid(font, text.c_str(), font_color);
    if (!font_surface) {
        std::cerr << "Failed to create text surface for font: " << TTF_GetError() << std::endl;
        exit(1); // Or handle the error as needed
    }

    font_texture = SDL_CreateTextureFromSurface(font_renderer, font_surface);
    if (!font_texture) {
        std::cerr << "Failed to create text texture: " << SDL_GetError() << std::endl;
        exit(1); // Or handle the error as needed
    }

    // Initialize the font_rect with the size of the font_surface
    font_rect.x = 0;
    font_rect.y = 0;
    font_rect.w = font_surface->w;
    font_rect.h = font_surface->h;
}

Font::~Font() {
    if (font_surface) {
        SDL_FreeSurface(font_surface);
    }
    if (font_texture) {
        SDL_DestroyTexture(font_texture);
    }
    if (font) {
        TTF_CloseFont(font);
    }
}

void Font::render(int x, int y) {
    font_rect.x = x;
    font_rect.y = y;
    SDL_RenderCopy(font_renderer, font_texture, nullptr, &font_rect);
}

void Font::setText(const std::string &new_text) {
    if (font_surface) {
        SDL_FreeSurface(font_surface); // Free the old surface
        font_surface = nullptr;
    }
    if (font_texture) {
        SDL_DestroyTexture(font_texture); // Destroy the old texture
        font_texture = nullptr;
    }

    font_surface = TTF_RenderText_Solid(font, new_text.c_str(), font_color);
    if (!font_surface) {
        std::cerr << "Failed to create text surface for new text: " << TTF_GetError() << std::endl;
        exit(1); // Or handle the error as needed
    }

    font_texture = SDL_CreateTextureFromSurface(font_renderer, font_surface);
    if (!font_texture) {
        std::cerr << "Failed to create text texture for new text: " << SDL_GetError() << std::endl;
        exit(1); // Or handle the error as needed
    }

    // Update the font_rect with the new size of the font_surface
    font_rect.w = font_surface->w;
    font_rect.h = font_surface->h;
}