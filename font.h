//
// Created by deca on 2/20/24.
//

#ifndef LEVELEDITOR_FONT_H
#define LEVELEDITOR_FONT_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <string>

class Font {
public:
    Font(const char *font_path, const std::string &text, SDL_Color color, int size, SDL_Renderer *renderer);

    ~Font();

    void render(int x, int y);

    void setText(const std::string &new_text);

    SDL_Surface *font_surface;

private:
    TTF_Font *font;
    SDL_Color font_color;
    std::string font_text;
    SDL_Texture *font_texture;
    SDL_Renderer *font_renderer;
    SDL_Rect font_rect;
};

#endif //LEVELEDITOR_FONT_H
