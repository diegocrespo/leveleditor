//
// Created by deca on 2/20/24.
//

#include "player.h"
#include <iostream>
Player::Player(float initX, float initY, int initWidth, int initHeight, SDL_Color initColor, float speed, SDL_Renderer* initRenderer)
        : x(initX), y(initY), width(initWidth), height(initHeight), color(initColor), speed(speed), renderer(initRenderer) {}

Player::~Player() {

}

void Player::draw() {
    SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
    SDL_Rect rect = {static_cast<int>(x), static_cast<int>(y), width, height};
    SDL_RenderFillRect(renderer, &rect);
}

void Player::update(float delta_time) {
    if (direction.up) y -= speed * delta_time;
    if (direction.down) y += speed * delta_time;
    if (direction.left) x -= speed * delta_time;
    if (direction.right) x += speed * delta_time;
}

void Player::handleKeyEvent(SDL_Scancode code, bool is_key_down) {
        switch (code) {
            case SDL_SCANCODE_W:
                direction.up = is_key_down;
                break;

            case SDL_SCANCODE_S:
                direction.down = is_key_down;
                break;

            case SDL_SCANCODE_A:
                direction.left = is_key_down;
                break;

            case SDL_SCANCODE_D:
                direction.right = is_key_down;
                break;

            default:
                break;
        }
}
