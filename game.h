//
// Created by deca on 2/20/24.
//

#ifndef LEVELEDITOR_GAME_H
#define LEVELEDITOR_GAME_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include "player.h"

enum GAME_STATE {
    MENU,
    PLAYING,
    GAME_OVER
};

class Game {
public:
    Game(const char* title, int width, int height);
    ~Game();
    void run();
    void handleInput();
    bool isRunning() const { return running; }

private:
    SDL_Window* window = nullptr;
    SDL_Renderer* renderer = nullptr;
    GAME_STATE state = MENU;
    bool running = false;

    bool initSDL();
    bool createWindowAndRenderer(const char* title, int width, int height);
    void handleKeyEvent(SDL_KeyboardEvent& keyEvent, bool isKeyDown);
    void update(float deltaTime);
    void render();
};
#endif //LEVELEDITOR_GAME_H
