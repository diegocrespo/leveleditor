// Completed 02/04/24
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include "font.h"
#include "player.h"
#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 640
#define FPS 60
#define MY_FONT "/usr/share/fonts/truetype/freefont/FreeSans.ttf"

enum GAME_STATE {
    MENU,
    PLAYING,
    GAME_OVER
};

class Game {
public:
    SDL_Window *window = nullptr;
    SDL_Renderer *renderer = nullptr;
    GAME_STATE state = MENU;
    bool running = true;

    // Constructor
    Game(const char *title, int width, int height) {
        if (initSDL() && createWindowAndRenderer(title, width, height)) {
            std::cout << "Game initialized successfully." << std::endl;
        } else {
            std::cout << "Game initialization failed." << std::endl;
        }
    }

    ~Game() {
        SDL_DestroyWindow(window);
        SDL_DestroyRenderer(renderer);
        TTF_Quit();
        SDL_Quit();
    }

    void handleInput(Player& player) {
        SDL_Event event;
        while (SDL_PollEvent(&event) !=0) {
            switch (event.type) {
                case SDL_QUIT:
                    state = GAME_OVER;
                    break;
                case SDL_KEYDOWN:
                    handleKeyEvent(player, event.key, true);
                    break;

                case SDL_KEYUP:
                    handleKeyEvent(player, event.key, false);
                    break;
            }
        }
    }

private:
    bool initSDL() {
        /*
         * Handles initialization of SDL and all of its submodules
         */
        if (SDL_Init(SDL_INIT_VIDEO) < 0) {
            std::cerr << "SDL could not initialize! SDL_Error: " << SDL_GetError() << std::endl;
            return false;
        }

        if (TTF_Init() < 0) {

            std::cerr << "SDL_ttf could not initialize! TTF_Error: " << TTF_GetError() << std::endl;
            SDL_Quit();
        }

        return true;
    }

    bool createWindowAndRenderer(const char *title, int width, int height) {
        /*
         * Handles the proper initialization of the window and the renderer and sets the variables
         */
        window = SDL_CreateWindow(title, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height,
                                  SDL_WINDOW_SHOWN);
        if (!window) {
            std::cerr << "Window could not be created successfully! SDL_Error: " << SDL_GetError() << std::endl;
            return false;
        }

        renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

        if (!renderer) {
            std::cerr << "Renderer could not be created successfully! SDL_Error: " << SDL_GetError() << std::endl;
            return false;
        }
        return true;
    }

    void handleKeyEvent(Player& player, SDL_KeyboardEvent &event, bool is_key_down) {
        if (event.repeat == 0) {
            switch(event.keysym.scancode){
                case SDL_SCANCODE_Y:
                    if (state != PLAYING) {
                        state = PLAYING;
                    }
                    break;

                case SDL_SCANCODE_N:
                    if (state != PLAYING) {
                        state = GAME_OVER;
                    }
                    break;

                case SDL_SCANCODE_ESCAPE:
                    if (state != PLAYING) {
                        state = GAME_OVER;
                    }
                    break;

                default:
                    std::cout << "Default Case!" << std::endl;
                    player.handleKeyEvent(event.keysym.scancode, is_key_down);
                    break;
            }
        }
    }
};

int main() {
    Game game = Game("Level Editor", SCREEN_WIDTH, SCREEN_HEIGHT);
    // Framerate independence
    Uint32 frame_start, frame_time;
    Uint32 last_frame_time = SDL_GetTicks();
    const int frame_delay = 1000 / FPS; // Calculating the time for each frame

    SDL_Color start_color = {0, 0, 0, 255};
    SDL_Color p_color = {255, 0, 0, 255};

    Font start_font = Font(MY_FONT, "Play Game? (Y/N)", start_color, 64, game.renderer);

    float speed = 500.0f;
    Player player(100.0, 100.0, 100, 50, p_color, speed, game.renderer);

    while (game.running) {
        frame_start = SDL_GetTicks();
        float delta_time = (frame_start - last_frame_time) / 1000.0f; // Convert milliseconds to seconds
        last_frame_time = frame_start;

        game.handleInput(player);
        SDL_SetRenderDrawColor(game.renderer, 255, 255, 255, 0);
        SDL_RenderClear(game.renderer);

        switch (game.state) {

            case MENU:
                start_font.render((SCREEN_WIDTH / 2) - (start_font.font_surface->w / 2),
                                  (SCREEN_HEIGHT / 2) - (start_font.font_surface->h / 2));
                SDL_RenderPresent(game.renderer);
                break;

            case PLAYING:
                player.update(delta_time);
                player.draw();

                SDL_RenderPresent(game.renderer);
                break;

            case GAME_OVER:
                std::cout << "Game Over" << std::endl;
                game.running = false;
                break;

            default:
                std::cout << "This state should never be reached" << std::endl;
                break;
        }
        // If the frame finished early, delay the next frame
        if (frame_delay > frame_time) {
            SDL_Delay(frame_delay - frame_time);
        }
    }
    return 0;
}